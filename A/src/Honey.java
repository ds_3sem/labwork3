public class Honey {
    private final int N;
    private static int current = 0;

    public Honey(int N) {
        this.N = N;
    }

    synchronized boolean isFull() {
        return current == N;
    }

    public synchronized void addPortion(int number) {
        while (isFull()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        current++;
        System.out.println("\nBee " + number + " brought a portion of honey! Honey " + current + "/" + N);
    }

    synchronized void honeyEmpty() {
        current = 0;
        System.out.println("All honey's eaten");
    }
}
