public class Bear implements Runnable {
    private final Honey honey;
    private volatile boolean isAsleep;

    public Bear(Honey honey) {
        this.honey = honey;
        this.isAsleep = true;
    }

    public synchronized void wakeBearUp() {
        isAsleep = false;
        notify();
    }

    @Override
    public void run() {
        while (true) {
            synchronized (this) {
                while (isAsleep) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                honey.honeyEmpty();
                isAsleep = true;
                System.out.println("Winnie is asleep again now");
                notifyAll();
            }
        }
    }
}
