import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class Main {
    public static void main(String[] args) {
        int n = 10;
        int N = 15;

        Honey honey = new Honey(N);
        Bear bear = new Bear(honey);

        new Thread(bear).start();

        ExecutorService executors = Executors.newFixedThreadPool(n);
        for (int i = 1; i <= n; i++) {
            Runnable bee = new Bee(honey, bear, i);
            executors.execute(bee);
        }

        executors.shutdown();
    }
}