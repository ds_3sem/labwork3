public class Bee implements Runnable {
    private final Honey honey;
    private final Bear bear;
    private final int number;

    public Bee(Honey honey, Bear bear, int number) {
        this.honey = honey;
        this.bear = bear;
        this.number = number;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            honey.addPortion(number);

            if (honey.isFull()) {
                System.out.println("\nBee " + number + " is waking Winnie up");
                bear.wakeBearUp();
            }

        }
    }
}
