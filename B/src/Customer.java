public class Customer implements Runnable {
    private final Hairdresser hairdresser;
    public int number;

    public Customer(Hairdresser hairdresser, int number) {
        this.hairdresser = hairdresser;
        this.number = number;
    }

    @Override
    public void run() {
        hairdresser.addToQueue(this);
    }
}
