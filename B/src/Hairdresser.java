import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class Hairdresser implements Runnable {
    private final Semaphore hairdresserSem = new Semaphore(1);
    private final Semaphore customerSem = new Semaphore(0);
    private final Queue<Customer> customers = new ConcurrentLinkedQueue<>();

    public void addToQueue(Customer customer) {
        customers.add(customer);
        if (hairdresserSem.availablePermits() == 0)
            System.out.println("Customer " + customer.number + " has joined the queue!");
        customerSem.release();
    }

    @Override
    public void run() {
        while (true) {
            try {
                customerSem.acquire();
                hairdresserSem.acquire();
                Customer current = customers.poll();
                int number = current.number;
                System.out.println("Customer " + number + " is in the chair");
                Thread.sleep(1000);
                System.out.println("Hairdresser has finished with Customer " + number + "!");
                hairdresserSem.release();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}