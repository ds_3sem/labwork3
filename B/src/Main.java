import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int customersAmount = 20;

        Thread[] threads = new Thread[customersAmount];
        Hairdresser hairdresser = new Hairdresser();

        Thread hairdresserThread = new Thread(hairdresser);
        hairdresserThread.start();

        for (int i = 1; i <= customersAmount; i++) {
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(250,  2000));
            } catch(InterruptedException e) {
                e.printStackTrace();
            }

            threads[i - 1] = new Thread(new Customer(hairdresser, i));
            threads[i - 1].start();
        }

        hairdresserThread.join();

        for (int i = 0; i < customersAmount; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}