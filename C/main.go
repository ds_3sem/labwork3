package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	tobacco = 0
	paper   = 1
	matches = 2
)

var components = []string{"тютюн", "папір", "сірники"}
var needed int

func newComponent() (int, int) {
	component1, component2 := rand.Intn(3), rand.Intn(3)
	for component1 == component2 {
		component2 = rand.Intn(3)
	}
	return component1, component2
}

func smoker(component int, componentNeeded *int, smokerSem chan bool, agentSem chan bool, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		smokerSem <- true
		if component == *componentNeeded {
			time.Sleep(time.Second / 2)
			fmt.Printf("\nКурець %d запалює цигарку", component+1)
			time.Sleep(time.Second)
			fmt.Printf("\nКурець %d закінчив палити", component+1)
			agentSem <- true
		} else {
			<-smokerSem
		}
	}
}

func agent(smokerSem chan bool, agentSem chan bool, wg *sync.WaitGroup) {
	defer wg.Done()
	for {
		<-agentSem
		var component1, component2 = newComponent()
		needed = 3 - component1 - component2
		time.Sleep(time.Second)
		fmt.Printf("\n\nПосередник поклав на стіл %s і %s", components[component1], components[component2])
		<-smokerSem
	}
}

func main() {
	var wg sync.WaitGroup
	var smokerSem, agentSem = make(chan bool), make(chan bool, 1)

	agentSem <- true
	wg.Add(1)

	go agent(smokerSem, agentSem, &wg)

	for i := 0; i < 3; i++ {
		switch i {
		case tobacco:
			fmt.Printf("Курець %d має %s", i+1, components[tobacco])
		case paper:
			fmt.Printf("\nКурець %d має %s", i+1, components[paper])
		case matches:
			fmt.Printf("\nКурець %d має %s", i+1, components[matches])
		}
	}

	for i := 0; i < 3; i++ {
		wg.Add(1)
		go smoker(i, &needed, smokerSem, agentSem, &wg)
	}

	wg.Wait()
}
